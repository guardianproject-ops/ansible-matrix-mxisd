ansible-matrix-mxisd
=========

Deploys [mxisd](https://github.com/kamax-matrix/mxisd) to a debian server.


Requirements
------------

* Debian

Role Variables
--------------

.dev releases are downloaded directly from github.

https://github.com/kamax-matrix/mxisd/releases

```
mxisd_version: 1.2.0
mxisd_hostname: 
mxisd_postgres_uri:
```

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: ansible-matrix-mxisd, mxisd_version: 1.3.0 }

Testing
-------

```
molecule test
```

License
-------

[GNU Affero General Public License (AGPL) v3+](https://www.gnu.org/licenses/agpl-3.0.en.html)

Author Information
------------------

```
Abel Luck <abel@guardianproject.info>
Guardian Project https://guardianproject.info
```

